/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define BUFFER_SIZE 4096
#define READ_FROM_STDIN 1
#define READ_FROM_FILE 2

/**
 * Prints a range of lines in a text file
 *
 * Jordan Sterling
 * Copyright 01/06/2014
 */


/**
 * Prints help
 */
void help() {
    printf("\nUsage:\trange [start line] [end line] [file]\n\n");
    printf("\tStart line and end line range is inclusive\n");
    printf("\tThe file name can be omitted to read over stdin\n");
}

/**
 * Finds and validates the presence of start line number and end line number. End can't be smaller than start.
 *
 * @param  int      argc    The number of arguments to the process
 * @param  char **  argv    All command line arguments to the process
 * @param  long *   start   Output parameter: The starting line number
 * @param  long *   end     Output parameter: The ending line number
 * @return bool             True if the line numbers are valid, False otherwise
 */
bool get_line_number_args(const int argc, char ** const argv, long *start, long *end) {
    bool success = true;

    if (argc < 3) {
        fprintf(stderr, "ERROR: Incorrect number of arguments\n");
        success = false;
    }
    else {
        char *err; /* holds the error location if strol fails */
        *start = strtol(argv[1], &err, 10);
        if (*err != '\0') {
            fprintf(stderr, "ERROR: Starting line number invalid");
            success = false;
        }
        else {
            *end = strtol(argv[2], &err, 10);
            if (*err != '\0') {
                fprintf(stderr, "ERROR: Ending line number invalid");
                success = false;
            }
            else if (*start > *end) {
                fprintf(stderr, "ERROR: Starting line number must be smaller than ending line number\n");
                success = false;
            }
        }
    }

    return success;
}

/**
 * there's 2 modes:
 *    1. process the file piped in over stdin
 *    2. process the file named in argment number 4
 */
int determine_input_mode(const int argc) {
    if (argc == 3) {
        return READ_FROM_STDIN;
    }
    return READ_FROM_FILE;
}

/**
 * Read file and print lines between the range to stdout
 * @param FILE file       The stream to read
 * @param long startLine  The line number to begin printing
 * @param long endLine    The line number to end printing
 */
void print_range(FILE *file, const unsigned long startLine, const unsigned long endLine) {
    unsigned long line = 1;     /* the line we are on */
    int i;                      /* a counter for the buffer */
    char c;                     /* the current character */
    int bytesRead;              /* bytes read into the buffer */
    bool reading = true;        /* control to still read from file */
    char buffer[BUFFER_SIZE];   /* buffer */
    while (reading) {
        bytesRead = fread(buffer, sizeof(char), BUFFER_SIZE, file);
        if (bytesRead != BUFFER_SIZE) {
            reading = false;
        }

        for (i = 0; i < bytesRead; i++) {
            c = buffer[i];
            if (line > endLine) {
                reading = false;
                break;
            }
            if (line >= startLine) {
                printf("%c", c);
            }
            if (c == '\n') {
                line++;
            }
        }
    }
}

/**
 * Finds and validates the presence of start line number and end line number. End can't be smaller than start.
 *
 * @param  int      argc    The number of arguments to the process
 * @param  char **  argv    All command line arguments to the process
 * @return char *           A string containing the filename on success, NULL otherwise
 */
char *get_input_filename(const int argc, char ** const argv) {
    if (argc < 4) {
        return NULL;
	}
    return argv[3];
}

/**
 * Range printer!
 */
int main(const int argc, char ** const argv) {
    unsigned long startLine;
    unsigned long endLine;
    char *filename;
    if (!get_line_number_args(argc, argv, &startLine, &endLine)) {
        help();
        return 1;
    }

    switch (determine_input_mode(argc)) {
        case READ_FROM_STDIN:
            print_range(stdin, startLine, endLine);
            break;

        case READ_FROM_FILE:
        default:
            /* line number args are validated but the filename has not been checked */
            filename = get_input_filename(argc, argv);
            if (!filename) {
                help();
                return 1;
            }
            FILE *file = fopen(filename, "r");
            if (file == NULL) {
                perror("Error while opening file");
                return 2;
            }

            print_range(file, startLine, endLine);
            if (fclose(file) != 0) {
                perror("Error while closing file");
			}
            break;
    }
    return 0;
}
