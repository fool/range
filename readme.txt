Prints a range of lines in a text file.

Jordan Sterling 1/6/14


Examples:


$ cat file.txt
1
2
3
4
5
6
7
8
9
10

$ ./range 1 2 file.txt
1
2

$ ./range 2 6 file.txt
2
3
4
5
6

$ cat file.txt | ./range 2 6
2
3
4
5
6

